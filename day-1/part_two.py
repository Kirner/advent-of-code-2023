VALID_WORDS = [
    ("one",   "1"),
    ("two",   "2"),
    ("three", "3"),
    ("four",  "4"),
    ("five",  "5"),
    ("six",   "6"),
    ("seven", "7"),
    ("eight", "8"),
    ("nine",  "9"),
]


def part_two(raw_input: str):
    lines = raw_input.splitlines()
    return sum([
        int(_first_digit(line) + _last_digit(line))
        for line in lines])


def _first_digit(str: str):
    for i, c in enumerate(str):
        if c.isdigit():
            return c
        if v := _get_number_for_word(str[i:]):
            return v


def _last_digit(str: str):
    for i in range(len(str))[::-1]:
        if str[i].isdigit():
            return str[i]
        if v := _get_number_for_word(str[i:]):
            return v


def _get_number_for_word(str):
    for (word, value) in VALID_WORDS:
        if not str.startswith(word):
            continue
        return value