def part_one(raw_input: str):
    lines = raw_input.splitlines()
    return sum([
        int(_first_digit(line) + _first_digit(line[::-1]))
        for line in lines])


def _first_digit(str):
    return next(
        c for c in str
        if c.isdigit())