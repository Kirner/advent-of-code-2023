class Node():
    def __init__(
            self,
            neighbor_coordinates,
            coordinates,
            raw_map):
        self.pipe = _pipe_at(coordinates, raw_map),
        self.neighbor_coordinates = neighbor_coordinates
        self.coordinates = coordinates
        self._raw_map = raw_map

    def __repr__(self): return self.__str__()

    def __str__(self):
        x, y = self.coordinates
        return f"<Node>(pipe: '{self.pipe}', x: {x}, y: {y})"

    @staticmethod
    def build(cords: (int, int), raw_map: list[str]):
        if (not cords
            or not (neighbors := _get_neighbor_cords(cords, raw_map))):
            return None

        return Node(neighbors, cords, raw_map)
    
    @property
    def neighbors(self):
        return [
            node
            for cord in list(self.neighbor_coordinates)
            if ((node := Node.build(cord, self._raw_map))
                and self.coordinates in node.neighbor_coordinates)]


def build_node_map(raw_input: str):
    raw_map = raw_input.splitlines()
    starting_cords = next(
        (row,col)
        for row, line in enumerate(raw_map)
        for col, pipe in enumerate(line)
        if pipe == "S")

    return Node.build(starting_cords, raw_map)


def _get_neighbor_cords(cords: (int, int), raw_map: list[str]):
    north = (cords[0], cords[1]-1)
    east = (cords[0]+1, cords[1])
    south = (cords[0], cords[1]+1)
    west = (cords[0]-1, cords[1])
    match _pipe_at(cords, raw_map):
        case '|': return (north, None, south, None)
        case '-': return (None, east, None, west)
        case 'L': return (north, east, None, None)
        case 'J': return (north, None, None, west)
        case '7': return (None, None, south, west)
        case 'F': return (None, east, south, None)
        case 'S': return (north, east, south, west)
        case '.': return None


def _pipe_at(cords: (int, int), raw_map: list[str]):
    x, y = cords
    if (y < 0
        or x < 0
        or y >= len(raw_map)
        or x >= len(raw_map[y])):
        return '.'

    return raw_map[y][x]
