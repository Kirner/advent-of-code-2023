from common import build_node_map


def part_one(raw_input: str):
    starting_node = build_node_map(raw_input)
    return _breadth_first_search(starting_node)


def _breadth_first_search(starting_node):
    neighbors = starting_node.neighbors
    visited = {}
    steps = 0
    while non_visited := _non_visited(neighbors, visited):
        visited.update({n.coordinates: True for n in non_visited})
        neighbors = [
            n
            for node in non_visited
            for n in node.neighbors]
        steps += 1
    return steps


def _non_visited(nodes, visited):
    return [
        node
        for node in nodes 
        if not visited.get(node.coordinates)]