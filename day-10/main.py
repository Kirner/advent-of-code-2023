from part_one import part_one
from part_two import part_two


def main():
    with open("input.txt", "r") as f:
        input = f.read()

    print("Part One Anwser: ", part_one(input))
    print("Part Two Anwser: ", part_two(input))


if __name__ == "__main__":
    main()