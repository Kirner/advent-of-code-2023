from common import build_sequence_tree, process_input


def part_one(raw_input: str):
    processed_input = process_input(raw_input)
    predictions = [_find_prediction_for_sequence(line) for line in processed_input]
    return sum(predictions)


def _find_prediction_for_sequence(sequence):
    tree = build_sequence_tree(sequence)
    tree.reverse()

    anwser = 0
    for i in range(len(tree)-1):
        prediction_for_layer = tree[i][-1] + tree[i+1][-2]
        tree[i+1].append(prediction_for_layer)
        anwser += prediction_for_layer

    return anwser