from functools import reduce
from re import split


def build_sequence_tree(sequence, prevous_sequences=[]):
    gaps = []
    if not prevous_sequences:
        prevous_sequences = [sequence]

    for i in range(len(sequence)-1):
        gaps.append(sequence[i+1] - sequence[i])

    if not _is_all_zeros(gaps):
        return build_sequence_tree(gaps, prevous_sequences + [gaps])

    return prevous_sequences + [gaps]


def process_input(raw_input: str):
    lines = raw_input.splitlines()

    # Remove comments for testing
    lines = [l for l in lines if l.strip()[0] != '#']

    return [
        [int(n) for n in split(r"\s+", l)]
        for l in lines]


def _is_all_zeros(sequence):
    if len(sequence) == 1:
        return sequence[0] == 0
    is_not_all_zeros = lambda x, y: bool(x) or bool(y)
    return not reduce(is_not_all_zeros, sequence)